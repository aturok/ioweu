﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace IoweU.Converters
{
    public class DateToStringConverter : IValueConverter
    {
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			var date = value as DateTime?;
			if (date != null)
				return String.Format("{0:d}", date);
			else
				return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}
