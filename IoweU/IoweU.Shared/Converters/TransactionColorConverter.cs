﻿using System;
using System.Collections.Generic;
using System.Text;
using IoweU.Model;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace IoweU.Converters
{
    public class TransactionColorConverter : IValueConverter
    {
		private Brush _debitBrush;
		private Brush _creditBrush;

		public object Convert(object value, Type targetType, object parameter, string language)
		{
			var transaction = value as Transaction;
			if (transaction == null)
				return null;

			return transaction.DebitCredit > 0 ? _debitBrush : _creditBrush;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}

		public TransactionColorConverter()
		{
			_debitBrush = new SolidColorBrush(Colors.Green);
			_creditBrush = new SolidColorBrush(Colors.Red);
		}
	}
}
