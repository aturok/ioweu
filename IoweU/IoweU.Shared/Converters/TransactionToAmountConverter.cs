﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

using IoweU.Model;

namespace IoweU.Converters
{
    public class TransactionToAmountConverter : IValueConverter
    {
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			var transaction = value as Transaction;

			if (transaction == null)
				return null;

			return String.Format("{0:n2}", transaction.Amount * transaction.DebitCredit);
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotImplementedException();
		}
	}
}
