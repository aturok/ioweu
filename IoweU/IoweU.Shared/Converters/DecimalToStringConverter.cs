﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace IoweU.Converters
{
    public class DecimalToStringConverter : IValueConverter
    {
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			return String.Format("{0:n2}", value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			var str = value as string;
			if (String.IsNullOrWhiteSpace(str))
				return 0.0m;

			decimal result = 0.0m;
			decimal.TryParse(str, out result);
			return result;
		}
	}
}
