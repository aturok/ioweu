﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using IoweU.Model;

namespace IoweU.Services.Impl
{
    public class TransactionsService : ITransactionsService
    {
		public async Task Add(Transaction transaction)
		{
			await _storage.Prepend(transaction);
		}

		public async Task<IEnumerable<Transaction>> Get()
		{
			return await _storage.Load();
		}

		private IListStorage<Transaction> _storage;
		public TransactionsService(IListStorage<Transaction> storage)
		{
			_storage = storage;
		}
	}
}
