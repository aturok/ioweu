﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Windows.Storage;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace IoweU.Services.Impl
{
    public class FileListStorage<ItemType> : IListStorage<ItemType>
    {
		private StorageFolder Folder { get { return ApplicationData.Current.LocalFolder; } }
		private string _filename;

		public async Task<List<ItemType>> Load()
		{
			try
			{
				using(var file = await Folder.OpenStreamForReadAsync(_filename))
				{
					var serializer = new DataContractSerializer(typeof(List<ItemType>));
					var list = (List<ItemType>)serializer.ReadObject(file);
					return list;
				}
			}
			catch(FileNotFoundException)
			{
				return new List<ItemType> { };
			}
		}

		public async Task Append(ItemType item)
		{
			await AddItem(item, (l, i) => l.Add(i));
		}

		public async Task Prepend(ItemType item)
		{
			await AddItem(item, (l, i) => l.Insert(0, i));
		}

		private async Task AddItem(ItemType item, Action<List<ItemType>, ItemType> adder)
		{
			var list = await Load();
			adder(list, item);

			using (var file = await Folder.OpenStreamForWriteAsync(_filename, CreationCollisionOption.ReplaceExisting))
			{
				var serializer = new DataContractSerializer(typeof(List<ItemType>));
				serializer.WriteObject(file, list);
			}
		}

		public FileListStorage(string filename)
		{
			_filename = filename;
		}
	}
}
