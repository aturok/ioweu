﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using IoweU.Model;

namespace IoweU.Services
{
    public interface ITransactionsService
    {
		Task Add(Transaction transaction);
		Task<IEnumerable<Transaction>> Get();
    }
}
