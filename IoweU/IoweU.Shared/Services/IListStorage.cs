﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

namespace IoweU.Services
{
    public interface IListStorage<ItemType>
    {
		Task<List<ItemType>> Load();
		Task Append(ItemType item);
		Task Prepend(ItemType item);
    }
}
