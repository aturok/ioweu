﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoweU.Model
{
    public class Transaction
    {
		public const int DebitSign = 1;
		public const int CreditSign = -1;

		public int DebitCredit { get; set; }

		public bool IsDebit { get { return DebitCredit == DebitSign; }}

		public bool IsCredit { get { return DebitCredit == CreditSign; } }

		public decimal Amount { get; set; }
		public string Person { get; set; }
		public DateTime? Date { get; set; }
    }
}
