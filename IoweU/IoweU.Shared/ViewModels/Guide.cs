﻿using System;
using System.Collections.Generic;
using System.Text;
using Caliburn.Micro;
using Windows.ApplicationModel.Resources;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;

namespace IoweU.ViewModels
{
    public class Guide
    {
		public virtual void ShowRecordHelp()
		{
			ShowMessage("HelpRecord");
		}

		public virtual void ShowReverseHelp()
		{
			ShowMessage("HelpReverse");
			DisableHelp();
		}

		private void ShowMessage(string messageKey)
		{
			var messageBox = new MessageDialog(ResourceLoader.GetString(messageKey));
			messageBox.ShowAsync();
		}

		private ResourceLoader ResourceLoader
		{
			get
			{
				return new Windows.ApplicationModel.Resources.ResourceLoader();
			}
		}

		private static bool IsHelpEnabled
		{
			get
			{
				if(Settings.ContainsKey(_HelpEnabledSettingKey) == false)
				{
					Settings[_HelpEnabledSettingKey] = true;
				}
				return (bool)Settings[_HelpEnabledSettingKey];
			}
		}

		private void DisableHelp()
		{
			Settings[_HelpEnabledSettingKey] = false;
		}

		private const string _HelpEnabledSettingKey = "IsHelpEnabled";

		private static IPropertySet Settings
		{
			get
			{
				return Windows.Storage.ApplicationData.Current.LocalSettings.Values;
			}
		}

		protected Guide() { }

		public static Guide GetGuide()
		{
			if (IsHelpEnabled)
				return new Guide();
			else
				return new FakeGuide();
		}

		public class FakeGuide : Guide
		{
			public override void ShowRecordHelp() { }
			public override void ShowReverseHelp() { }
		}
    }
}
