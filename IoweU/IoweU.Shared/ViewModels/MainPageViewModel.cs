﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using IoweU.Model;
using IoweU.Services;
using Windows.ApplicationModel.Contacts;
using Windows.UI.Popups;

namespace IoweU.ViewModels
{
	public class MainPageViewModel : Screen
	{
		private int _DebitCredit;
		public int DebitCredit
		{
			get { return _DebitCredit; }
			set
			{
				_DebitCredit = value;
				NotifyOfPropertyChange(() => DebitCredit);
				NotifyOfPropertyChange(() => IsDebit);
				NotifyOfPropertyChange(() => IsCredit);
			}
		}

		public bool IsDebit
		{
			get { return DebitCredit == Transaction.DebitSign; }
			set { DebitCredit = value ? Transaction.DebitSign : Transaction.CreditSign; }
		}
		public bool IsCredit
		{
			get { return DebitCredit == Transaction.CreditSign; }
			set { DebitCredit = value ? Transaction.CreditSign : Transaction.DebitSign; }
		}

		public string Person { get; set; }

		public decimal Amount { get; set; }

		public async Task Save()
		{
			if (String.IsNullOrWhiteSpace(Person))
			{
				TellError("TransactionPersonMissing");
				return;
			}
			if (Amount == 0)
			{
				TellError("TransactionZeroAmount");
				return;
			}

			var transaction = new Transaction
			{
				DebitCredit = DebitCredit,
				Amount = Amount,
				Person = Person,
				Date = DateTime.Now
			};

			await _transactionService.Add(transaction);
			Transactions.Insert(0, transaction);
			NotifyOfPropertyChange(() => TotalDebit);
			NotifyOfPropertyChange(() => TotalCredit);
			NotifyOfPropertyChange(() => TotalBalance);
			NotifyOfPropertyChange(() => BalanceByPerson);

			Guide.GetGuide().ShowReverseHelp();
		}

        public PersonCreator PersonCreator { get; private set; }

		public async void PickContact()
		{
            PersonCreator.IsActive = true;
		}

		public ObservableCollection<Transaction> Transactions { get; set; }

		public IEnumerable<Transaction> Balances
		{
			get
			{
				if (Transactions == null)
					return new List<Transaction> { };

				return Transactions.GroupBy(t => t.Person)
					.Select(transactions => new Transaction
					{
						Person = transactions.Key,
						Amount = transactions.Sum(t => t.Amount * t.DebitCredit)
					})
					.Select(t => { t.DebitCredit = Math.Sign(t.Amount); t.Amount = Math.Abs(t.Amount); return t; })
					.ToList();
			}
		}


		private ObservableCollection<string> _availablePersons;
		public ObservableCollection<string> AvailablePersons
		{
			get
			{
				if(_availablePersons == null)
				{
					var persons = Transactions == null ? new List<string> { } : Transactions.Select(b => b.Person).Distinct();
					_availablePersons = new ObservableCollection<string>(persons.OrderBy(p => p));
				}

				return _availablePersons;
			}
		}

		public IEnumerable<Transaction> BalanceByPerson
		{
			get
			{
				return Balances
					.Where(t => t.Amount != 0.0m)
					.OrderBy(t => t.Person)
					.ToList();
			}
		}

		public Transaction TotalDebit
		{
			get
			{
				if (Balances == null)
					return new Transaction { Amount = 0.0m, DebitCredit = 1 };
				else
				{
					var debit = Balances.Sum(t => t.IsDebit ? t.Amount : 0.0m);
					return new Transaction { Amount = debit, DebitCredit = Transaction.DebitSign };
				}
			}
		}

		public Transaction TotalCredit
		{
			get
			{
				if (Balances == null)
					return new Transaction { Amount = 0.0m, DebitCredit = 1 };
				else
				{
					var credit = Balances.Sum(t => t.IsCredit ? t.Amount : 0.0m);
					return new Transaction { Amount = credit, DebitCredit = Transaction.CreditSign };
				}
			}
		}

		public Transaction TotalBalance
		{
			get
			{
				if (Balances == null)
					return new Transaction { Amount = 0.0m, DebitCredit = 1 };
				else
				{
					var balance = TotalDebit.Amount - TotalCredit.Amount;
					return new Transaction { Amount = Math.Abs(balance), DebitCredit = Math.Sign(balance) };
				}
			}
		}

		public void Reverse(Transaction transaction)
		{
			IsDebit = transaction.IsCredit;
			Amount = transaction.Amount;
			Person = transaction.Person;

			NotifyOfPropertyChange(() => Amount);
			NotifyOfPropertyChange(() => Person);
		}

		private ITransactionsService _transactionService;

		public MainPageViewModel(ITransactionsService transactionService)
		{
			_transactionService = transactionService;
            PersonCreator = new PersonCreator(this);

			DebitCredit = Transaction.DebitSign;
		}

		protected async override void OnInitialize()
		{
			base.OnInitialize();

			Transactions = new ObservableCollection<Transaction>(await _transactionService.Get());
			NotifyOfPropertyChange("");
		}

		public async void TellError(string messageKey)
		{
			var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
			var message = loader.GetString("Error_" + messageKey);
			await new MessageDialog(String.IsNullOrEmpty(message) ? messageKey : message).ShowAsync();
		}

		protected override void OnActivate()
		{
			base.OnActivate();

			Guide.GetGuide().ShowRecordHelp();
		}
	}
}
