﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Caliburn.Micro;
using Windows.ApplicationModel.Contacts;

namespace IoweU.ViewModels
{
    public class PersonCreator : PropertyChangedBase
    {
        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                NotifyOfPropertyChange(() => IsActive);
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public ICommand AddCommand { get { return new ActionCommandHandler(() => Add()); } }

        public void Add()
        {
            if(String.IsNullOrWhiteSpace(Name) == false &&
                _target != null && _target.AvailablePersons != null &&
                _target.AvailablePersons.Contains(Name) == false)
            {
                _target.AvailablePersons.Add(Name);
                _target.Person = Name;
                _target.NotifyOfPropertyChange("Person");

                IsActive = false;
                Name = "";
            }
        }

        public ICommand PickContactCommand { get { return new ActionCommandHandler(() => PickContact()); } }

        public async void PickContact()
        {
            var picker = new ContactPicker();
            picker.DesiredFieldsWithContactFieldType.Add(ContactFieldType.PhoneNumber);
            var contact = await picker.PickContactAsync();

            if (contact != null)
            {
                Name = PersonNameFromContact(contact);
            }
        }

        private string PersonNameFromContact(Contact contact)
        {
            return string.Join(" ", new string[] { contact.FirstName, contact.LastName });
        }

        private MainPageViewModel _target;

        public PersonCreator(MainPageViewModel target)
        {
            _target = target;
        }
    }
}
