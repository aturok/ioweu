﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace IoweU.ViewModels
{
    public class ActionCommandHandler : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (_action != null)
            {
                _action();
            }
        }

        private System.Action _action;
        public ActionCommandHandler(System.Action action)
        {
            _action = action;
        }
    }
}
