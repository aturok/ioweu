﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

using Caliburn.Micro;

using IoweU.Services;
using IoweU.Services.Impl;
using IoweU.ViewModels;

namespace IoweU
{
	public sealed partial class App
	{
		private WinRTContainer container;

		public App()
		{
			this.InitializeComponent();
		}

		protected override void Configure()
		{
			container = new WinRTContainer();
			container.RegisterWinRTServices();

			RegisterViewModels();
			RegisterServices();
		}

		protected void RegisterViewModels()
		{
			container.PerRequest<MainPageViewModel>();
		}

		private void RegisterServices()
		{
			container.PerRequest<ITransactionsService, TransactionsService>();

			var transactionsStorage = new FileListStorage<Model.Transaction>("transactions.xml");
			container.Instance<IListStorage<Model.Transaction>>(transactionsStorage);
		}

		protected override void PrepareViewFirst(Frame rootFrame)
		{
			container.RegisterNavigationService(rootFrame);
		}

		protected override void OnLaunched(LaunchActivatedEventArgs args)
		{
			DisplayRootViewFor<MainPageViewModel>();
		}

		protected override object GetInstance(Type service, string key)
		{
			return container.GetInstance(service, key);
		}

		protected override IEnumerable<object> GetAllInstances(Type service)
		{
			return container.GetAllInstances(service);
		}

		protected override void BuildUp(object instance)
		{
			container.BuildUp(instance);
		}
	}
}